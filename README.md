# HackySeg Data

In diesem Repository befindet sich der für das Projekt HackySeg erstellte Testdatensatz. Dieser besteht aus acht ausgewählten Videos und zwölf kurzen Clips. Bei den Clips handelt es sich um 2-10 Sekunden lange Auschnitte aus den Videos, die Szenen beinhalten an denen sich die Funktionalität von HackySeg evaluieren lässt. Des Weiteren ist eine Datei mit vortrainierten Parametern für das genutzte neuronale Netzwerk enthalten.
